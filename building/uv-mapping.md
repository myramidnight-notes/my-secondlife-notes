* [Specular maps in SecondLife tutorial](https://nwn.blogs.com/nwn/2016/06/specular-maps-in-second-life-tutorial.html)
* [Normal and Specular mapping](https://community.secondlife.com/knowledgebase/english/materials-normal-and-specular-mapping-r1352/)
# UV maps

So UV maps are simply textures that can give the shader specific information, where the colors of the map have specific meaning.
* _specular/shininess mapping_
* _normal/bumpiness mapping_
* _scuplt mapping_
    >The image maps the positions of the vertices of the shape.

### Terms
It might be useful what things are called, to understand what you might be mapping:
* __diffuse__: it is essentially just the texture/color on a matte surface, so this does not include the glossiness (that is calculated through specularity)
* __specular__: it is the glossiness/shininess. 
* __normal__: vecotrs that tell the shader in what direction the surface is facing


## Specular mapping (shininess/glossiness)
>how glossy is the surface? Where is it glossy? Just a grayscale image, white would be full glossiness, black meaning no glossiness.
## Normal mapping (bumpiness)
>in what directions is the surface facing, allows you to add 3D details to a flat sufrace.

What a UV map does essentially is to give a flat 2D surface the information it needs to appear as if 3D. You would not really notice until you tried to view it at an odd angle in most cases. Basically __fake 3D__. It gives a mesh object more depth without having to spend it on extra polygons for the shape.

    


#### Normals?
>Normal vector tells the shader in what direction that particular surface is facing, which decides how it is going to shade it based on how the light hits the surface. So normal mapping can give it that information, instead of just calculating it based on the normal of the polygon.


## Trivia
#### UV and XY
>The U and V in the name actually stands for the alternative names for the axis of the camera, so it's like saying it's a XY texture (telling us that it's only got two axis, 2D). 
>
>The alternative names for the axis is for the camera, which is the center of the universe as far as the shader can tell, everything is rendered from it's point of view. So we give the camera axis different names from the world/model axis. The X,Y and Z axis become U, V and N in the camera. 
>
>I guess we hardly have to think about it, since we mostly pay attention to the world axis (XYZ) when placing things into the world and positioning them. But I thought it would be interesting to know about this.
