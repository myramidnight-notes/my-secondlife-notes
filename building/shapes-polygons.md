# Shapes on Second Life
1. Initially there were only __prims__ (short for primitives), various standard geometric shapes that you could manipulate and piece together to create 
1. Later the __sculpts__ arrived. They were 3D modeled shapes that used UV mapping of their vertices to tell the program how to render the shape.
1. Finally we got __mesh__ on SL, allowing us to upload shapes we make in blender or other 3D modeling programs. 

It was a great thing, but only if you understood the importance of polygons. Uploading something with an absurdly high polygon count is actually defeats the point of trying to reduce the polygon count with the use of mesh.  

#### Polygons?
>They are the triangles (well, it is typically a triangle) that compose the object, and we can get amazing details without having too many polygons. The magic happens with __UV bump maps__.

## Primatives (prims)
They are the basic building blocks of SL, and have a max size you can stretch them to (essentially 10 meters). The different types of prims have different options to manipulate them, and you can get very interesting shapes only using primatives. 

The main issue with being restricted to primatives is that you tend to need a lot of them to create any complex shapes, and when you have a lot of prims, the object gets rather _expensive_ (the price is essentially polygon count).

Twisting primatives into shapes could be considered an art form almost.

## Sculpts
This used to be the only way to get custom shapes other than primatives on Second Life. They used a UV map of the vertex positions, so the object could only be as complex as that 2D texture could allow you. 

## Mesh
It is relatively recently that Second Life allowed players to upload mesh. Some consider it as a way to keep SL alive, because it was quickly becoming very outdated compared to the various game engines out there. 

It allowed players to upload more complex and precice shapes than what sculpts allowed, and when one keeps polygons in mind, mesh was also a way to reduce the overall polygon count in the world, resulting in less lag while rendering.

This reduction of lag only works if creators keep polygons in mind, to keep the polygons to a minimum and make use of UV normal maps to get the most out of their models. But there are products out there that rival anything else in polygon count, actually causing more lag than reducing it.

> Be proactive and pay attention to how 'expensive' mesh is to render, in order to raise awareness of the fact that polygons matter.

Easiest way to see if something is _expensive_ to render, polygon wise, is to see the world as a wire mesh. If you can actually clearly see the orignal mesh, even it's colors, while viewing wireframe, then it's being absurd on it's polygon count.

Usually people create the mesh with high polygon count, which is just normal, but when exporting it should be lowered (essentially simplifying the shape) and extra polygons could be used to simply create a normal map that preserves the 3D details of the model.

### Polygons 
[Polygons](https://en.wikipedia.org/wiki/Polygon) are flat shapes that compose the model, usually defined as triangles. 

Because they matter when everything needs to be rendered constantly, you want to make it as _cheap_ as possible to render. And the more polygons the shader needs to process, the more expensive it is to render (both time and processing). 

We essentially want just enough of the polygons to keep the shape, too few will blur the shape into a blob. __normal mapping__ allows you to retain the details without exessive polygons in the final model (the export). 

>You don't want your wireframe to look like this:
>
>![](images/too-many-polygons.png)
>
>Wireframe is supposed to show only the outlines of polygons, but when you can essentially see the original model while in wire-frame, then you know something is wrong (as in, horribly optimized, too many polygons). Those stockings are probably more expensive to render than the entire collection of objects that make up the sim.

## Normal mapping

>#### What is a normal?
>It is a vector that tells the shader in what direction the surface is facing, so it can shade it accordingly (calculate how much light reaches that surface).)

A __normal map__ is simply a texture that the shader/render program will translate into normals for the model that the texture is applied to. So instead of having to generate the normals based on the polygons, we can just provide it in the form of a colored image, every pixel being mapped onto the surface and the color translated to a vector pointing in a specific direction. 

Could call it "fake 3D" since the surface could be completely flat, and still be rendered as if it wasn't flat (you wouldn't notice unless looking at the object from certain angles). It wouldn't be the same as just coloring in shading, it would be actual shadows calculated by the shader program based on the normals.

![](images/normal-map.jpg)
1. Left: Original Model (4 million polygons/triangles) 
1. Center: low polygon version of the model (500 polygons/triangles) 
1. Right: The low-polygon model rendered with normal map of the original model applied to it.

You can get a lot of details without going overboard with the polygons, just by using normal maps.

>#### Another example of a normal map applied to a flat surface
>![](images/normal-map-example.png)
>1. Left: Original model (since it's actually 3D, it can cast shadows)
>1. Center: The normal map generated from the model
>1. Right: A flat surface rendered with the normal map applied. It shades the surface according to the normal map, as if it had a 3D shape. Sure you could just paint a surface to look like that, but the point is that the shadows are not painted on. They will change depending on the light source. Since it is a flat surface, it does not actually cast shadows, only the actual shape/model casts shadows (if the shader is set to generate shadows, that is)
>
>![](images/normal-map-example-moving-light.gif)
>* Here is the same example, with a moving light to show the difference, on the left is the original 3D model, and on the right is a flat surface with the normal map applied.
>* [Normal mapping (wikipedia)](https://en.wikipedia.org/wiki/Normal_mapping)