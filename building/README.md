# Building in Second Life
Well, everything you see within SL happens to be created by players, it is amazing what we can do. So why not get in on the action and make something!


## Index
* ### [Shapes and Polygons](shapes-polygons.md)
    > Primatives, Sculpts, Mesh and the notion of polygons.

* ### [UV mapping](uv-mapping.md)
    > Using textures to give the rendering/shader directions on how the object should appear: 
    >* specular(glossiness)
    >* normals (bumpiness) 
    >* positions (for the shape of sculpts)