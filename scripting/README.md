# Scripting in SL
Second Life has it's own scripting language, called __Linden Scripting Language__ (_LSL_ for short). It's structure is largely based on Java and C.

Main Sources:
* [Getting started with LSL](http://wiki.secondlife.com/wiki/Getting_started_with_LSL)
* [LSL 101 (secondlife wiki)](http://wiki.secondlife.com/wiki/LSL_101)


## Index
* [Hello World](Hello_world.md)
* [LSL basics](basics.md)
* [Events](events.md)