# Teaching Scripting
To teach myself! I've never really done proper programming in LSL before

### LSL syntax
>I have noticed that LSL is about as strict about syntax as you'd expect from C, the most basic errors you might get tend to be related to a forgotten `;`
>
>It is equally strict about you forgetting `;`, as it is about you adding them in the wrong places.
>
>The `;` essentially tells your program "you can execute this piece of code". Though you do not add `;` after defining states, functions or conditional statements.
### Code/Text editors
>I would reccomend using a good coding editor to do the coding, rather than doing it in-world, if not only for the the ability to add extentions that help the auto-complete of code and making your life so much easier.
>
>I happen to use Visual Studio Code (VSCode for short, not to be confused with _Visual Studio_), and I found an extention called `VSCode LSL` to add to it.

## First script
We got to start somewhere, and SecondLife actually can create your intial "Hello World" script for you, it's like the basic starting place to test the waters.

### Hello World/Avatar!
When you create a new object in the world, you can actually go into it's `contents` and find a __new script__ button waiting for you. This will create a typical "hello world" script, which is a very good place to start.

Here is the code (with added comments)
```lsl
// Default state
// It is the 'main' of the script
default
{
    // Do something when we enter the state
    state_entry()
    {
        //Say "Hello, Avatar!" on channel 0 (global)
        llSay(0, "Hello, Avatar!"); 
    }

    // Have object do something when touched
    touch_start(integer total_number)
    {
        //Say "Touched." on channel 0 (global)
        llSay(0, "Touched.");
    }
}
```
* So LSL revolves around states and events, and the initial state is `default`, which is essentially the __main__ function you would expect when coding in a C programming language.
* It has some inbuilt __event listener__ functions, which will run the code within their braces if the event is triggered.
    * `state_entry` listens for when we enter the state, do something then. Here it is basically "do something when you finish loading the script"
    * `touch_start` listens for the beginning of a touch, when you click an object. The `touch_end` would be the oposite, listening for when you stop touching the object, release the mouse.

## States
As mentioned before, there is the state named __default__ that has the same purpose as the __main__ function of some other programming languages. This is where the program starts running.

### Adding your own states
You can define your own states. We shall take the previous example and introduce a new state:
```lsl
default
{
    state_entry()
    {
        llSay(0, "Hello, Avatar!");
    }

    // Do something when touched
    touch_start(integer total_number)
    {
        // enter the 'talk' state
        state talk;
    }
}

// The new state, named 'talk'
state talk
{
    // Do something when touch ends
    touch_end(integer total_number)
    {
        llSay(0, "Touched.");
    }
}
```

## Functions
### Pre-defined functions
There are pre-defined functions for LSL, that are identified by the `ll` in the beginning of their names. The most basic one, which can be seen in the _Hello world_ example, would be `llSay` that allows the script to _speak_ into the chat.
```
llSay(0,"Hello, Avatar!")
```
* `0` is the chat channel it speaks into, 0 being the global channel
* next comes the string message that gets sent to the channel.

They do many fun things, and you might browse existing scripts or the wiki to research those.

### Make your own functions
If you have programmed in C or something similar, then functions in LSL are defined in a similar manner. The main difference is the fact that if the function returns nothing, you do not have to specify a return type.

```lsl
my_custom_function(string message)
{
    //Code to say the string into public channel
    llSay(0,"Hello, Avatar!")
}
 
default
{
    state_entry()
    {
        my_custom_function("Hello world!");
    }
}
```