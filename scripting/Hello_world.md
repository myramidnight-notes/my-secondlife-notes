
* [Getting started with LSL](http://wiki.secondlife.com/wiki/Getting_started_with_LSL)

## Creating objects
The simplest and most basic thing you can create on Second Life is an object, in a place that allows you to _rez_ things (render things). You can place scripts into these objects to make them do things.

* Found an extenstion called `VSCode LSL` (If you would like to write code in VSCode and have some autocomplete action)

## New script (Hello World)
The default script that gets created when you press __new script__  within the __content__ tab of your object, this is what you get:
```lsl
// Default state
// It is the 'main' of the script
default
{
    // Do something when we enter the state
    state_entry()
    {
        //Say "Hello, Avatar!" on channel 0 (global)
        llSay(0, "Hello, Avatar!"); 
    }

    // Have object do something when touched
    touch_start(integer total_number)
    {
        //Say "Touched." on channel 0 (global)
        llSay(0, "Touched.");
    }
}
```

It's a decent place to start
* The `state_entry` is a good way to have the object notify you somehow that it has finished loading the script. Script won't do anything if it hasn't loaded yet naturally.
* You can add more states, besides for the default (but you will always need `default` to be there)
    ```lsl
    state new_state
    {
        llSay(0,"Touched again.")
    }
    ```

>Still I currently have no clue why 
## Using states
Let's just change the previous example, and move part of the code into a new state
```lsl
default
{
    state_entry()
    {
        llSay(0, "Hello, Avatar!");
    }

    // Do something when touched
    touch_start(integer total_number)
    {
        // enter the 'talk' state
        state talk;
    }
}

// The new state, named 'talk'
state talk
{
    // Do something when touch ends
    touch_end(integer total_number)
    {
        llSay(0, "Touched.");
    }
}
```
* Now it will enter the `talk` state when you touch the object
* It will say `Touched.` when you stop touching the object

## Talking functions!
Well, as the above examples show, you can make scripts talk to you in Second Life. But you can also make them _whisper_ or talk only to the owner of the object, or only talk to a specific channel. There are many things you can do with simply talking.
* `llSay()`
* `llWhisper()`
* `llOwnerSay()`

## Floating Text functions
You can use `llSetText` to add a floating text to the object, which usually disappears at a certain distance (only to be seen when you're near it). 
```lsl
llSetText("I have been touched!",<1.0,1.0,1.0>, 1);
```
1. First comes the text
1. Next comes the font color (RGB)
1. Finally comes the alpha level

This hover text can linger even if you remove the script, so you can use this line with a empty string to remove stuck hover text.

## Coloring object functions
You can use `llSetColor` to color the object, and you specify what side you wish to apply the color to.
```
llSetColor(<.5, .5, .5>, ALL_SIDES);
```
1. First comes the color (the numbers are in the range of 0 to 1). If you happen to know a RGB color, but it's in the 255 range, simply divide it with 255 to get it in the range of 0 to 1.
