# Events
So it seems that coding in SL is all about __events__ (things that happen, triggers) and __states__. So we can have a _touch_ event (someone touching the object) and that triggers something to happen. 

To keep things organized, we could have it trigger a change of state, such as a open door will become closed, and implement how it closes. The primary initial state is called __Default__ (which would be _main_ function if you were programming in `C`, which is where your program will start).

So instead of organizing things into classes and functions, we'll organize them into _states_, specially when our script is complex.

There are special functions built into LSL 

### Event types
There are alot of types of event types
* touch events 
* events in chat channel (listen)
* sensory events
* Collision events (did we just hit someone/thing?)
* transactions (getting money)
* [...list of events types](https://wiki.secondlife.com/wiki/Category:LSL_Events)
