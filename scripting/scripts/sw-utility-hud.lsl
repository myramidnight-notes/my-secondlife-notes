//My first proper LSL script that I made on my own :D (MyraMidnight)
//Allows Seawolf dragons to trigger certain features without having to remember lots of gesture triggers
//or having to carry the customization HUD around everywhere when you only want these few features

integer getLinkNum(string primName)
{
    integer primCount = llGetNumberOfPrims();
    integer i;
    for (i=0; i<primCount+1;i++)
    {
        if (llGetLinkName(i)==primName) return i;
    }
    return i;
}        

command(string message) {
    llSay (37226,message);
}

say (string message) {
    llOwnerSay (message);
}
      
bar_default()
{

    if (face == 0) //Wiki
        llOwnerSay("See extensive documentation about the Seawolf Dragon avatars here: "
        +"http://www.seawolf-monsters.com/wiki/Dragon/Dragon");
    if (face == 1) //Saddle (owner of HUD needs to insert saddle object manually)
    llRezObject("Dragon Saddle", llGetPos() + <0.00000, 2.99999, -0.00000>, <0.0,0.0,0.0>, <0.0,0.0,0.0,1.0>, 0);
    if (face == 2) //Neck toggle
        command("ntog");
    if (face == 3) //Autofly
        command("xfly");
    if (face == 4) //Sit
        command("hsit");
    if (face == 5) //Flapping toggle
        command("hflap");
    if (face == 6) // Tail toggle
        command("htail");                
    if (face == 7) //Camera toggle
        command("c");
}

integer face;
default
{
    touch_start(integer num_detected)
    {
        face = llDetectedTouchFace(0);
        integer link = llDetectedLinkNumber(0);

        integer row_0 = getLinkNum("minimized"); //The basic functions (when minimized)
        integer row_back = getLinkNum("basic_back"); //The basic functions (on back)
        integer row_1 = getLinkNum("basic0"); //The basic functions (on front)
        integer row_2 = getLinkNum("basic1"); //first row of basic functions
        integer row_3 = getLinkNum("basic2"); //second row of basic functions
        integer row_4 = getLinkNum("basic3"); //third row of basic functions
        integer row_5 = getLinkNum("basic4"); //third row of basic functions
        integer min_front =   getLinkNum("min_front"); //the buttons to minimize
        integer min_back =   getLinkNum("min_back"); //the buttons to minimize
        integer max =   getLinkNum("max"); //the buttons to maximize

//Minimize and flip (front)
        if (link == min_front) {
            if (face == 5)         //minimize
                llSetLinkPrimitiveParamsFast(1,[PRIM_ROT_LOCAL,llEuler2Rot(<0.0,-PI/2,0>)*llGetLocalRot()]);       
            else if (face == 0)         //flip
                 llSetLinkPrimitiveParamsFast(1,[PRIM_ROT_LOCAL,llEuler2Rot(<0.0,0,PI/1>)*llGetLocalRot()]);
            
        }
        
//Minimize and flip (back)
        if (link == min_back) {
            if (face == 5)         //minimize
                llSetLinkPrimitiveParamsFast(1,[PRIM_ROT_LOCAL,llEuler2Rot(<0.0,PI/2,PI/1>)*llGetLocalRot()]); 
            else if (face == 0)         //flip
                 llSetLinkPrimitiveParamsFast(1,[PRIM_ROT_LOCAL,llEuler2Rot(<0.0,0,PI/1>)*llGetLocalRot()]);  
          
        }        
//Maximize 
        if (link == max) {
            if (face == 5)         //maximize to custom gestures
                llSetLinkPrimitiveParamsFast(1,[PRIM_ROT_LOCAL,llEuler2Rot(<0.0,PI/2,PI/1>)*llGetLocalRot()]); 
            if (face == 0)         //maximize to basic functions
                 llSetLinkPrimitiveParamsFast(1,[PRIM_ROT_LOCAL,llEuler2Rot(<0.0,PI/2,0>)*llGetLocalRot()]);           
        }

//Button functions on minimized (and top row)
        if (link == row_0 || link == row_1 || link == row_back) {
            bar_default()
        }
           
// First row of basic functions
        if (link == row_2) {
            if (face == 0) //expression: smile
                command("msmile");
            if (face == 1) //expression: neutral
                command("mneutral");
            if (face == 2) //expression: grin
                command("mgrin");
            if (face == 4) //eye open fully
                command("i1");
            if (face == 5) //eye open
                command("i2");
            if (face == 6) //eye closed
                command("i3");
            if (face == 7) //fire breath
                command("rfire");
        }
        
// Second row of basic functions
        if (link == row_3) {
            if (face == 0) //expression: smirk
                command("msmirk");
            if (face == 1) //expression: not amused
                command("mnotamused");
            if (face == 2) //expression: mad
                command("mmad");
            if (face == 4) //mouth closed
                command("j1");
            if (face == 5) //mouth open
                command("j2");
            if (face == 6) //mouth open fully
                command("j3");
            if (face == 7) //ice breath
                command("rice");

         }           
// Third row of basic functions
        if (link == row_4) {
            if (face == 1) //expression: sad
                command("msad");
            if (face == 4) // toes curl: hands
                command("h1");
            if (face == 5) // neck up
                command("nup");
            if (face == 6) // growl 1
                command("o1");

        }
// Forth row of basic functions
        if (link == row_5) {
            if (face == 0) // state: sit
                command("sit");
            if (face == 1) // state: swim
                command("swim");
            if (face == 2) // state: walk
                command("walk");
            if (face == 3) // state: fly
                command("fly");
            if (face == 4) // toes curl: feet
                command("h2");
            if (face == 5) // neck down
                command("ndown");
            if (face == 6) // growl 2
                command("o2");
            if (face == 7) // voice
                command("voice");

        }
    
    
    }
}

