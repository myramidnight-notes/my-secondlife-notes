// 3-Button HUD Shell
// by Reality Control 2009-10-22
// Last Updated: 2013-02-11 (incorporating fix by Barrington John)
// License: Public Domain (but a credit in your product documentation would be nice)

// We need to remap the faces to a more logical order...

default
{
    touch_end(integer num_detected)
    // Not the most robust way of handling the touch event.
    // If this object was rezzed in world, it would be a problem
    // but HUDs can only be touched by the wearer
    {
        integer touchedFace = llDetectedTouchFace(0); 
        llOwnerSay("this is face "+(string)(touchedFace));
        // or you could assign it to a variable...
    }
}