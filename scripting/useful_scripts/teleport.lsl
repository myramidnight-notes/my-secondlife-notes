default
{
    state_entry()
    {
        llSetText("",<255,255,255>,1.0);
        llSetSitText("Teleport");
        llSitTarget(<-4,7,-15.6>, llEuler2Rot(<PI,0,0>));
    }
    
    changed(integer change)
     {
        llSleep(0.5); 
        llUnSit(llAvatarOnSitTarget());
    }
}