# Useful scripts
These can be very useful tools. I do not claim to have written them, most seem to include the information of who made them. Many were simply found on the SL wiki

> You just drop these scripts in for the desired effect, but you should be careful to __pay attention to what the script is intended for__, because they can effect the object they are in.

What better way to learn is there than looking at existing scripts? Figure out why they work.

## Index
* [__Identify object faces__](touch_face.lsl)
    > Need to identify the faces of an object? This script will tell you what face you just touched.
* [__Give inventory__](give_inventory.lsl)
    > Script that gives a person the first thing in the object inventory upon touch
* [__Teleport script__](teleport.lsl)
    >A script that teleports a person that sits on it (and it renames the option for sitting to be "teleport" instead)