# Basics
Well, more of a deeper look into how LSL works as a programming language. I am just comparing it to the languages I've come into contact with during my studies in Computer Science, which has been a interesting ride.

>LSL (short for Linden Scripting Language) is heavily based on Java and C, so if you are familiar with those languages, maybe you'll spot similarities. But it is still a unique programming language, just like how the shaders for OpenGL writes essentially like C, but having it's own elements and using only what it needs from the language it took inspiration from.

I have tried programming in C, more familiar with C++ and even done some projects using C#. 

## Data types
* [Variables (LSL 101)](http://wiki.secondlife.com/wiki/LSL_101/Variables)

We need to define data types for just about everything (return types, parameter types, variable types... )


So here is what we got
* `integer`
    >This covers booleans, since 1 and 0 are essentially true and false.
* `string`
* `float`
    >numbers with decimal places
* `key`
    > the UUID keys. It is a string with a specific format.
* `vector`
    >a 3D vector, written as: `<0,0,0>`. They are used for positions and colors (since RGB are just 3 numbers, just like a vector).
* `rotation`
    >4 floats: __x,y,z__ and __s__, written as : `<0.0, 0.0, 0.0, 1.0>`
* `list`
    > Our arrays, contained in square brackets, and they can actually contain varied values.

## User defined functions
Since there is a library of functions that LSL is made of, I got curious about how you would go about actually defining your own functions. 

There is no keywords that that allow you to define a function (like `def` used in _python_). You simply write the function name and it's parameters, then the body of the function. 

### Void functions (return nothing)
Unlike `C`, you don't have to specify if you are writing a `void` function (a function that returns nothing):
```lsl
let_prim_say_something_and_include_owner_info(string message)
{
    //Code to say the string into public channel
}
 
default
{
    state_entry()
    {
        let_prim_say_something_and_include_owner_info("Hello world!");
    }
}
```

Here is an example of a LSL user defined function that returns integer:
```lsl
integer is_sun_up(integer num)
{
    //Using a LSL function to get the direction of the sun
    vector sunDirection = llGetSunDirection();

    if (sunDirection.z < 0.0)
        return FALSE;

    else
        return TRUE;
}
 
default
{
    state_entry()
    {
        // Use the is_sun_up function
        if ( is_sun_up() )
            llSay(PUBLIC_CHANNEL, "Good day!");
        else
            llSay(PUBLIC_CHANNEL, "Good night!");
    }
}
```
* In programming, `FALSE` can simply be `0`, and any other value equals `TRUE` (it is like a question of _"does this integer have a value?"_ and translate that to True/False), those are essentially the same thing. That is why a function returning a _integer_ can return `TRUE` or `FALSE`, it's just integers. 

So when we are creating a function, we have to specify the __return type__, unless we want it to return nothing.