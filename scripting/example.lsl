
// Default state
default
{
    // Do something when script initially loads
    state_entry()
    {
        //Say "Hello, Avatar!" on channel 0 (global)
        llSay(0, "Hello, Avatar!"); 
    }

    // Have object do something when touched
    touch_start(integer total_number)
    {
        //Say "Touched." on channel 0 (global)
        llSay(0, "Touched.");
    }
}