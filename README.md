# Second life notes
I suddenly decided to make notes about coding and building in SL as I explore it (well, I was familiar with building before, but not scripting). It is interesting to figure out a new programming language, specially after studying computer science for a while. I hardly know what I'm doing when I begin these notes, but we can figure it out together.

Programming in SL is highly connected to building, because you place the scripts inside objects. So it is good to know a bit about the building interface before going into scripting.

>I use the __firestorm__ viewer, in case you wonder about the look of interfaces, but I doubt I will be using anything that isn't part of SL basic features, hard to tell when I'm not using the basic SL viewer.

## Index
* [Building](building/README.md)
* [Scripting](scripting/README.md)